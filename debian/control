Source: cpp-httplib
Section: libs
Priority: optional
Maintainer: Andrea Pappacoda <andrea@pappacoda.it>
Build-Depends:
 debhelper-compat (= 13),
 dpkg-build-api (= 1),
 dpkg-dev (>= 1.22.5),
Build-Depends-Arch:
 libbrotli-dev,
 libcurl4-openssl-dev | libcurl-dev,
 libgtest-dev <!nocheck>,
 libssl-dev (>= 3.0.0),
 meson (>= 0.62.0),
 openssl <!nocheck>,
 pkg-config,
 python3:any,
 zlib1g-dev,
Standards-Version: 4.7.0
Homepage: https://github.com/yhirose/cpp-httplib
Vcs-Git: https://salsa.debian.org/debian/cpp-httplib.git
Vcs-Browser: https://salsa.debian.org/debian/cpp-httplib
X-Style: black

Package: libcpp-httplib0.18
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: C++ HTTP/HTTPS server and client library
 cpp-httplib is a C++11 cross platform HTTP/HTTPS library, with a focus on
 ease of use. This is a multi-threaded 'blocking' HTTP library. If you are
 looking for a 'non-blocking' library, this is not the one that you want.

Package: libcpp-httplib-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 libbrotli-dev,
 libcpp-httplib0.18 (= ${binary:Version}),
 libssl-dev,
 zlib1g-dev,
 ${misc:Depends},
Description: C++ HTTP/HTTPS server and client library - development files
 cpp-httplib is a C++11 cross platform HTTP/HTTPS library, with a focus on
 ease of use. This is a multi-threaded 'blocking' HTTP library. If you are
 looking for a 'non-blocking' library, this is not the one that you want.
 .
 This package contains the development headers and a pkg-config file.
